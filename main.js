const {app, BrowserWindow, ipcMain} = require('electron')
const {screen} = require('electron');
const ScreenRecorder = require("./screenRecorder").ScreenRecorder

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const path = require('path')
const fs = require('fs')

let nativeImage;
let screenRecorder;


let webFrameDimensions = {}
let mainWindow = null;
let displays = [];

const createMainWindow = () => {
    let mainWindowDimension = {};
    if (displays.length > 0) {
        let widthDependOnHeight = Math.ceil((displays[0].workArea.height - 80) * Math.sqrt(2));
        let width = Math.min(displays[0].workArea.width - 80, widthDependOnHeight);
        let height = Math.ceil((width * Math.sqrt(2) / 2));
        let x = Math.ceil((displays[0].workArea.width - width) / 2);
        let y = Math.ceil((displays[0].workArea.height - height) / 2);
        //console.log("widthDependOnHeight", widthDependOnHeight, "displays[0].workArea.height", displays[0].workArea.height, "width", width, "x", x, "displays[0].workArea.width", displays[0].workArea.width)

        mainWindowDimension.x = parseInt(x + "");
        mainWindowDimension.y = parseInt(y + "");
        mainWindowDimension.width = width;
        mainWindowDimension.height = height;
        console.log("xy", x, y);
    }
    console.log("mainWindowDimension", mainWindowDimension);
    app.commandLine.appendSwitch('disable-site-isolation-trials')


    mainWindow = new BrowserWindow({
        ...mainWindowDimension,
        resizable: false,
        minHeight: mainWindowDimension.height,
        minWidth: mainWindowDimension.width,
        movable: false,
        enableLargerThanScreen: true,
        webPreferences: {
            webSecurity: false,
            allowRunningInsecureContent: true,
            preload: path.join(__dirname, 'preload.js'),
            experimentalFeatures: true
        }
    })

    //mainWindow.setAlwaysOnTop(true, "floating", 3);
    //mainWindow.setVisibleOnAllWorkspaces(true);
    mainWindow.loadFile('index.html').then(() => {

    })

    mainWindow.webContents.session.webRequest.onHeadersReceived({urls: ["*://*/*"]},
        (d, c) => {
            if (d.responseHeaders['X-Frame-Options']) {
                delete d.responseHeaders['X-Frame-Options'];
            } else if (d.responseHeaders['x-frame-options']) {
                delete d.responseHeaders['x-frame-options'];
            }
            if (d.responseHeaders['content-security-policy']) {
                delete d.responseHeaders['content-security-policy'];
            }
            if (d.responseHeaders['Content-Security-Policy']) {
                delete d.responseHeaders['Content-Security-Policy'];
            }

            c({cancel: false, responseHeaders: d.responseHeaders});
        }
    );
    /*mainWindow.addListener('move', (event) => {
        let controlWindowBounds = event.sender.getBounds()
        let windowBounds = {};
        if (displays.length > 0) {
            windowBounds.x = controlWindowBounds.x;
            windowBounds.y = controlWindowBounds.y + 80;
            windowBounds.width = controlWindowBounds.width;
            windowBounds.height = Math.ceil(controlWindowBounds.width / Math.sqrt(2));
        }
        if (webpageWin != null && !webpageWin.isDestroyed())
            webpageWin.setBounds(windowBounds)
        //console.log("move", controlWindowBounds)
    })*/
    mainWindow.addListener('close', () => {
        if (screenRecorder && screenRecorder instanceof ScreenRecorder) {
            screenRecorder.stopRecording();
        }
    })
    /*mainWindow.addListener('minimize', (event) => {
        webpageWin.minimize();
    })
    mainWindow.addListener('maximize', (event) => {
        webpageWin.maximize();
    })*/

    /*mainWindow.addListener('resize', (event) => {
        let controlWindowBounds = event.sender.getBounds()
        let windowBounds = {};
        if (displays.length > 0) {
            windowBounds.x = controlWindowBounds.x;
            windowBounds.y = controlWindowBounds.y + 82;
            windowBounds.width = controlWindowBounds.width;
            windowBounds.height = Math.ceil(controlWindowBounds.width * Math.sqrt(2) / 2);
        }
        // if(webpageWin!= null && !webpageWin.isDestroyed())
        //     webpageWin.setBounds(windowBounds)
        //console.log("move", windowBounds)
    })*/

}

const ready = () => {

    displays = screen.getAllDisplays();
    //console.log("getAllDisplays", displays);

    app.on('activate', function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) createMainWindow()
    })

    createMainWindow();


    ipcMain.on('webFrameDimension', (_, wpd) => {
        //console.log("webPageDimension", wpd);
        webFrameDimensions = wpd
    })

    ipcMain.on('open', (_) => {
        /*let controlWindowBounds = mainWindow.getBounds();
        let height = Math.ceil(controlWindowBounds.width * Math.sqrt(2) / 2);
        console.log("height", height);
        if (webpageWin === null || webpageWin.isDestroyed()) {
            let windowDimension = {}
            if (displays.length > 0) {
                windowDimension.x = controlWindowBounds.x;
                windowDimension.y = controlWindowBounds.y + 82;
                windowDimension.width = controlWindowBounds.width;
                windowDimension.height = parseInt(Math.ceil(controlWindowBounds.width * Math.sqrt(2) / 2));
            }
            webpageWin = new BrowserWindow({
                ...windowDimension,
                enableLargerThanScreen: true,
                resizable: false,
                movable: false,
                frame: false,
                transparent: true,
                //parent: mainWindow,
                webPreferences: {
                    nodeIntegration: false, // is default value after Electron v5
                    contextIsolation: true, // protect against prototype pollution
                    enableRemoteModule: false, // turn off remote
                    preload: path.join(__dirname, 'webPagePreload.js'),
                },
            });

            //webpageWin.setAlwaysOnTop(true, "floating", 2);
            //webpageWin.setVisibleOnAllWorkspaces(true);
            console.log("webpageWin", webpageWin.id);
        }
        ipcMain.emit("webPageWinId", webpageWin.id);
        webpageWin.loadURL(url);
        webpageWin.once("page-title-updated", (_, title) => {
            webPageTitle = title;
        })*/
    })

    /*ipcMain.on("requestPageTitle", (event) => {
        event.reply("webPageWinTitle", webPageTitle)
    })*/

    /*ipcMain.on("titleChange", (event, title) => {
        webPageTitle = title;
    })*/

    ipcMain.on('capture', async () => {
        //console.log("1", new Date());
        /*const pageRect = await webpageWin.webContents.executeJavaScript(
            `(() => {  
              var body = document.body,
              html = document.documentElement;
              var height = Math.max( body.scrollHeight, body.offsetHeight, 
                       html.clientHeight, html.scrollHeight, html.offsetHeight );
              return {x: 0, y: 0, width: document.body.offsetWidth, height: height}
            })()`);*/
        //console.log("2", new Date());

        //console.log("pageRect", webFrameDimensions)

        try {
            let initBounds = mainWindow.getBounds();



            let controlNTitleBarHeight = initBounds.height - webFrameDimensions.height;

            console.log("initBounds.height", initBounds.height);
            console.log("webFrameDimensions", webFrameDimensions);
            let newBound = {
                x: initBounds.x,
                y: initBounds.y,
                width: webFrameDimensions.innerWidth + 120,
                height: (initBounds.height + (webFrameDimensions.innerHeight - webFrameDimensions.height))
            };
            console.log("Setting bound to: " ,newBound );
            console.log("controlNTitleBarHeight: " ,controlNTitleBarHeight );
            await mainWindow.setBounds(newBound);

            await sleep(2000);

            let image = await mainWindow.webContents.capturePage()
            await mainWindow.setBounds(initBounds);


            const PNGCrop = require('png-crop');
            let fileName = Date.now() + ".png";

            fs.writeFileSync(fileName, image.toPNG(), (err) => {
                if (err) throw err
            })


            let config1 = {
                height: webFrameDimensions.innerHeight,
                top: 0,
                left: 120,
                width: webFrameDimensions.innerWidth
            };
// pass a path, a buffer or a stream as the input
            PNGCrop.crop(fileName, fileName, config1, function (err) {
                if (err) throw err;
                //console.log('done!');
            });

            //console.log("4", new Date());
            //console.log("5", new Date());

        } catch (e) {
            console.log(e)
        }

    })


    ipcMain.on('startVideoRecording', () => {
        //mediaRecorder.start();
        if (!screenRecorder || !screenRecorder.recording) {
            let xid = mainWindow.getNativeWindowHandle().readUInt32LE();
            xid = xid.toString(16);
            console.log(`nativeWindowHandle: ${xid}`)
            screenRecorder = new ScreenRecorder(Date.now() + ".mp4", "0x" + xid);
            screenRecorder.startRecording().then(() => {
                console.log("VideoRecordingStarted")
            });
        } else {
            screenRecorder.stopRecording();
            console.log("VideoRecordingStopped")
        }
    })


    ipcMain.on('stopVideoRecording', async () => {
        //mediaRecorder.stop();
        screenRecorder.stopRecording();
        console.log("VideoRecordingStopped")
    })

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
    ready()
})


// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
})
