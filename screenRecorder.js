const {spawn, exec} = require('child_process');
const util = require('util');

class ScreenRecorder {

    windowXid;
    outputFilePath;
    audioDevice;
    recording = false

    constructor(outputFilePath, windowXid) {
        this.windowXid = windowXid;
        this.outputFilePath = outputFilePath;

    }

    opts = ["-qe", "ximagesrc", "xid=", "use-damage=false", "do-timestamp=true", "!",
        "queue", "leaky=2", "max-size-buffers=0", "max-size-time=0", "max-size-bytes=0", "!",
        "videorate", "!", "video/x-raw,framerate=20/1", "!",
        "videoconvert", "!", "queue", "!",
        "x264enc", "sliced-threads=true", "tune=zerolatency", "speed-preset=superfast", "bitrate=1024", "!",
        "mp4mux", "name=mux", "!",
        "filesink", 'location=', "sync=false", "pulsesrc", 'device=', "provide-clock=true", "do-timestamp=true", "buffer-time=40000", "!",
        "queue", "leaky=2", "max-size-buffers=0", "max-size-time=0", "max-size-bytes=0", "!",
        "audiorate", "skip-to-first=true", "!", "audio/x-raw,channels=2", "!", "audioconvert", "!",
        "queue", "!",
        "lamemp3enc", "!",
        "mux."
    ]

    process;

    stopRecording() {
        try {
            this.process.kill('SIGINT');
            this.recording = false;
        } catch (err) {
        }
    }


    async startRecording() {
        if (this.recording) {
            this.stopRecording();
        }

        const execPB = util.promisify(exec);
        if(!this.audioDevice) {
            const {stdout, stderr} = await execPB("pactl list | grep -A2 'Source #' | grep 'Name: ' | cut -d\" \" -f 2 | grep 'output'");
            this.audioDevice = stdout;
            console.log("audioDevice", this.audioDevice)
        }
        let stdio = ['ignore', 'pipe', 'pipe'];

        let opts = this.opts.map((item) => {
            switch (item) {
                case "location=":
                    return item + this.outputFilePath;
                case "xid=":
                    return item + this.windowXid;
                case "device=":
                    return item + this.audioDevice;
            }
            return item;
        })

        console.log("Final Command", "/usr/bin/gst-launch-1.0 " + opts.join(" "))
        this.process = spawn("/usr/bin/gst-launch-1.0", opts, {stdio: stdio, detached: true});
        this.process.once('close', () => {
            console.log("Process closed")
            this.process = null
        });
        this.process.once('error', (err) => console.error(err.message));
        this.recording = true;
        console.log("recording true", stdio);
        return true;
    }

}

module.exports = {ScreenRecorder};