#!/bin/bash

OutputSoundDev="$(pactl list | grep -A2 'Source #' | grep 'Name: ' | cut -d" " -f 2 | grep 'output')"

gst-launch-1.0 -qe ximagesrc xname="$1" use-damage=false do-timestamp=true !\
 queue leaky=2 max-size-buffers=0 max-size-time=0 max-size-bytes=0 !\
 videorate ! video/x-raw,framerate=20/1 ! videoconvert ! queue !\
 x264enc sliced-threads=true tune=zerolatency speed-preset=superfast bitrate=1024 !\
 mp4mux name=mux !\
 filesink location="$2" sync=false pulsesrc device="$OutputSoundDev" provide-clock=true do-timestamp=true buffer-time=40000 !\
 queue leaky=2 max-size-buffers=0 max-size-time=0 max-size-bytes=0 !\
 audiorate skip-to-first=true ! audio/x-raw,channels=2 ! audioconvert !\
 queue ! lamemp3enc ! mux.