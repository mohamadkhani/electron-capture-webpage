const {ipcRenderer} = require('electron')


const sendDimensions = () => {
    let body = document.body,
        html = document.documentElement;
    let height = Math.max(body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight);

    let width = Math.max(body.scrollWidth, body.offsetWidth,
        html.clientHeight, html.scrollWidth, html.offsetWidth);
    ipcRenderer.send('webPageDimension', {x: 0, y: 0, width: width, height: height})

}

ipcRenderer.send('webPageLoaded', document.title);

setInterval(()=>{
    ipcRenderer.send('titleChange', document.title);
},300)

window.addEventListener('DOMContentLoaded', () => {
    console.log("Dom Loaded");
    ipcRenderer.send('webPageLoaded', document.title);
    sendDimensions();
    let body = document.body,
        html = document.documentElement;
    setInterval(sendDimensions,1000);
    body.addEventListener("resize",sendDimensions)
    html.addEventListener("resize",sendDimensions)
    window.addEventListener("resize", sendDimensions);
    document.addEventListener("resize", sendDimensions);
});